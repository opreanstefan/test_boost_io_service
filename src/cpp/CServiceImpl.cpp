#include <iostream>

#include "hpp/CServiceImpl.hpp"

ServiceImpl::ServiceImpl()
{
    std::cout << "Constructor ServiceImpl" << std::endl;
}

void ServiceImpl::destroy()
{

}

void ServiceImpl::wait()
{
    std::cout << "wait function of ServiceImpl: Here I add the implementation of the service" << std::endl;
}

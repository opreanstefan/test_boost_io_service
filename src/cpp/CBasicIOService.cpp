#include "hpp/CBasicIOService.hpp"

template<> boost::asio::io_service::id BasicIOService< ServiceImpl >::id;

template<> BasicIOService< ServiceImpl >::BasicIOService(boost::asio::io_service& ioService) :
service(ioService),
m_asyncWork(new boost::asio::io_service::work(m_asyncIOService)),
m_asyncThread(boost::bind(&boost::asio::io_service::run, &m_asyncIOService))
{
    std::cout << "Constructor of BasicIOService< ServiceImpl >" << std::endl;
}

template<> BasicIOService< ServiceImpl >::~BasicIOService()
{
    std::cout << "Destructor of BasicIOService< ServiceImpl >" << std::endl;
    m_asyncWork.reset();
    m_asyncIOService.stop();
    m_asyncThread.join();
}

template<> void BasicIOService< ServiceImpl >::construct(ImplPtr& implIn)
{
    std::cout << "construct of BasicIOService< ServiceImpl > ??" << std::endl;
}

template<> void BasicIOService< ServiceImpl >::init(ImplPtr& implIn)
{
    std::cout << "Initialization of BasicIOService< ServiceImpl >" << std::endl;
}

template<> void BasicIOService< ServiceImpl >::destroy(ImplPtr& implIn)
{
    if (!implIn == nullptr)
    {
        implIn->destroy();
    }
    implIn.reset();
}

// TODO: impl of in-class + adding std::cout in functions to see that it works + add call in main
#include "hpp/CBasicIOObject.hpp"

#include <boost/asio/placeholders.hpp>
#include <boost/bind/bind.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>

template< class Service > BasicIOObject< Service >::BasicIOObject(boost::asio::io_service& ioServiceIn) :
boost::asio::basic_io_object< Service >(ioServiceIn)
{
    this->service.init(this->implementation);
}

template< class Service > void BasicIOObject< Service >::wait()
{
    this->service.wait(this->implementation);
}

template< class Service > template< class Handler > void BasicIOObject< Service >::async_wait(Handler handler)
{
    this->service.async_wait(this->implementation, handler);
}

template< class Service > void BasicIOObject< Service >::wait_handler(const boost::system::error_code& bsec)
{
    this->service.async_wait(
        this->implementation, boost::bind(&BasicIOObject::wait_handler, this, boost::asio::placeholders::error));
}

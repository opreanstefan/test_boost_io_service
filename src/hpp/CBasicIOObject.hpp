#pragma once

#include <boost/asio/basic_io_object.hpp>
// #include <boost/asio/io_service.hpp>

template< class Service > class BasicIOObject : public boost::asio::basic_io_object< Service >
{
public:
    explicit BasicIOObject(boost::asio::io_service& ioServiceIn); // other params needed, like config file name

    void wait();
    template< class Handler > void async_wait(Handler handler);
    void wait_handler(const boost::system::error_code& bsec);
};

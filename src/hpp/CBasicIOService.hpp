#pragma once

#include "hpp/CServiceImpl.hpp"

#include <boost/asio/io_service.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>

template< ServiceImpl > class BasicIOService : public boost::asio::io_service::service
{
private:
    boost::asio::io_service m_asyncIOService;
    boost::scoped_ptr< boost::asio::io_service::work > m_asyncWork;
    boost::thread m_asyncThread;

    void shutdown_service();

public:
    static boost::asio::io_service::id sm_id;

    explicit BasicIOService(boost::asio::io_service& ioService);
    ~BasicIOService();

    typedef boost::shared_ptr< ServiceImpl > ImplPtr;

    void construct(ImplPtr& implIn);
    void init(ImplPtr& implIn); // some other params that need to be used in the init, like config file name
    void destroy(ImplPtr& implIn);
    void wait(ImplPtr& implIn);

    template< class Handler > class WaitOperation
    {
    private:
        boost::weak_ptr< ServiceImpl > m_impl;
        boost::asio::io_service& m_ioService;
        boost::asio::io_service::work m_work;
        Handler m_handler;

    public:
        void WaitOperation(ImplPtr& implIn, boost::asio::io_service& ioServiceIn, Handler& handlerIn);

        void operator()() const;
    };

    template< class Handler > async_wait(ImplPtr& implIn, Handler& handlerIn);
};
